import sys
from flask import Flask, render_template, send_from_directory, jsonify
from flask_flatpages import FlatPages, pygments_style_defs

app = Flask(__name__)
FLATPAGES_EXTENSION = '.md'
FLATPAGES_ROOT = 'content'
FlatPages = FlatPages(app)
POST_DIR = "posts"

app.config.from_object(__name__)

@app.route("/")
def serverInfo():
    return jsonify({
        "name": "Game On Entertainment OnSight",
        "status": "inClusterReady",
        "dbmode": "flatpage",
        "debug": "probably"
    })

@app.route("/posts/")
def getPostListings():
    posts = [p for p in FlatPages if p.path.startswith(POST_DIR)]
    posts.sort(key=lambda item:item['date'], reverse=False)
    ephemeralDict = {}
    for obj in posts:
        print(obj)
        ephemeralDict[vars(obj)["path"]] = {
            "title": obj.meta["title"],
            "author": obj.meta["author"],
            "date": obj.meta["date"],
            # Flask-Flatpages says there is a way to get this in an object,
            # but it doesn't seem to work.
            "url": vars(obj)["path"]
        }
    return(ephemeralDict)

@app.route("/posts/<name>/")
def getPost(name):
    path = '{}/{}'.format(POST_DIR, name)
    post = FlatPages.get_or_404(path)
    ephemeralDict = {
        "metadata": {
            "title": post.meta["title"],
            "author": post.meta["author"],
            "date": post.meta["date"],
            "url": vars(post)["path"]
        },
        "markdownBody": post.body,
        "htmlBody": post.html
    }
    
    return ephemeralDict

# Staticfiles
@app.route('/static/<path:path>')
def serveSupportingFile(path):
    try:
        return send_from_directory('static', path)
    except:
        return jsonify({
            "attemptedFilepath": path,
            "errorCode": 404,
            "errorDescriptor": f"Sorry, I couldn't find a file with the filepath {path} on this site.",
        })

@app.after_request
def after_request(response):
    header = response.headers
    header['Access-Control-Allow-Origin'] = '*'
    return response

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)